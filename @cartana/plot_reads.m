function plot_reads(ctn)
% Plot on reference DAPI image
% Cartana, 2020, Xiaoyan


% a quick plot
figure; plotall(ctn.DecodedResults(~strcmp(ctn.DecodedResults(:,2), 'NNNN'),2),...
    cell2mat(ctn.DecodedResults(~strcmp(ctn.DecodedResults(:,2), 'NNNN'),3:4)),...
    ctn.AlignedImages{1,ctn.DAPIChannel(1)});



    function [uNames, cName] = plotall(name, pos, imfile, scale)
        % [uNames, cName] = plotall(name, pos, imfile, scale, second_imfile)
        % simply plot all reads
        % Xiaoyan, 2018
        
        % prepare background
        % figure;
        
        if ischar(imfile)
            try
                imfile = imread(imfile);
                imshow(imfile, []);
            catch
                axis image;
                set(gca, 'YDir', 'reverse', 'color', [.4 .4 .4]);
                axis off
                hold on;
            end
        else
            imshow(imfile, []);
        end
        
        if nargin <= 3
            scale = 1;
        end
        
        % unique genes
        [uNames, ~, iName] = unique(name);
        cName = hist(iName, 1:length(uNames));
        
        % correct between 0 and 0.5 difference between image and
        % coordinatess
        pos = (pos-1/scale/2+.5)*scale + 1;
        sym = repmat(symlist, ceil(length(uNames)/length(symlist)), 1);
        hold on
        for i = 1:length(uNames)
            plot(pos(iName==i,1), pos(iName==i,2), sym{i});
        end
        
        legend(uNames, 'color', [.6 .6 .6], 'location', 'NorthEastOutside');
        
    end

    function sym_list = symlist
        % Xiaoyan, 2017
        
        sym = {
            1 'r.';
            2 'g.';
            3 'y.';
            4 'm.';
            5 'c.';
            6 'w.';
            7 'b.';
            8 'r*';
            9 'g*';
            10 'y*';
            11 'm*';
            12 'c*';
            13 'w*';
            14 'b*';
            15 'ro';
            16 'go';
            17 'yo';
            18 'mo';
            19 'co';
            20 'wo';
            21 'bo';
            22 'rs';
            23 'gs';
            24 'ys';
            25 'ms';
            26 'cs';
            27 'ws';
            28 'bs';
            29 'r+';
            30 'g+';
            31 'y+';
            32 'm+';
            33 'c+';
            34 'w+';
            35 'b+';
            36 'rx';
            37 'gx';
            38 'yx';
            39 'mx';
            40 'cx';
            41 'wx';
            42 'bx';
            43 'r^';
            44 'g^';
            45 'y^';
            46 'm^';
            47 'c^';
            48 'w^';
            49 'b^';
            50 'rd';
            51 'gd';
            52 'yd';
            53 'md';
            54 'cd';
            55 'wd';
            56 'bd';
            57 'rp';
            58 'gp';
            59 'yp';
            60 'mp';
            61 'cp';
            62 'wp';
            63 'bp';
            64 'rh'};
        
        sym_list = sym(:,2);
        
    end
end