function ctn = prealign_sections(ctn)
% Pre-align big stitched images
% Alignment will be performed on downsized images
% This is only necessary if microscope stage repositioning is not accurate
% enough and there is big shift between ISS rounds
% Cartana, Xiaoyan, 2020

if isempty(ctn.DownsizeFactor)
    if ctn.PixelScale < .2
        ctn.DownsizeFactor = .1;
    else
        ctn.DownsizeFactor = .2;
    end
end

% always use the first input images as reference
RefImageFile = fullfile(ctn.StitchedImages{1,ctn.DAPIChannel(1)});

% read and prepare reference image
try
    RefImage = imread(RefImageFile);
catch ME
    if contains(ME.identifier, 'NotExist')
        load([RefImageFile '.mat']);
        if exist('transformed', 'var')
            RefImage = transformed;
            clear transformed
        else
            RefImage = I;
            clear I
        end
    else
        disp(ME.identifier);
    end
end

SizeRef = size(RefImage);
RefImageDownsized = imresize(RefImage, ctn.DownsizeFactor);
clear RefImage;


for r = 2: (ctn.nRounds + ctn.isLibraryPrep)
    outdir = fullfile(ctn.OutputDirectory, 'Aligned');
    if ~exist(outdir, 'dir')
        mkdir(outdir);
    end
    
    % registration matrix
    OutputFile = fullfile(outdir, ['iss' num2str(r-ctn.isLibraryPrep) '_d' num2str(ctn.DownsizeFactor) '.jpg']);
    FloatImageFile = fullfile(ctn.StitchedImages{r,ctn.DAPIChannel(r)});
    
    if ~exist(OutputFile, 'file')
        fprintf('%s Aligning %s\n', datetime, FloatImageFile);
        
        try
            FloatImage = imread(FloatImageFile);
        catch ME
            if contains(ME.identifier, 'NotExist')
                load([FloatImageFile '.mat']);
                if exist('transformed', 'var')
                    FloatImage = transformed;
                    clear transformed
                else
                    FloatImage = I;
                    clear I
                end
            else
                disp(ME.identifier);
            end
        end
        FloatImageDownsized = imresize(FloatImage, ctn.DownsizeFactor);
        clear FloatImage
        
        % use imregcorr function in Image Processing Toolbox
        TForm = imregcorr(FloatImageDownsized, RefImageDownsized, 'rigid');
        
        %         tform = dftregistration(fft2(RefImageDownsized), fft2(FloatImageDownsized), 2);
        %         TForm = affine2d;
%         TForm.T = [1 0 0; 0 1 0; tform(4) tform(3) 1];

        FusedAligned = imfuse(imwarp(FloatImageDownsized, TForm, 'OutputView',...
            imref2d(size(RefImageDownsized))), RefImageDownsized);
        
        imwrite(FusedAligned, OutputFile);
        csvwrite([OutputFile(1:end-3) 'csv'], TForm.T);
        clear FusedAligned
    else
        fprintf('%s %s already aligned\n', datetime, FloatImageFile);
    end
    
    % apply transformation matrix
    outdir = fullfile(ctn.OutputDirectory, 'Aligned', ['iss' num2str(r-ctn.isLibraryPrep)]);
    if ~exist(outdir, 'dir')
        mkdir(outdir);
    end
    
    for c = 1:size(ctn.StitchedImages,2)
        OutputFile = fullfile(outdir, ['c' num2str(c) '.tif']);
        if ~exist(OutputFile, 'file')
            
            TForm = csvread(fullfile(ctn.OutputDirectory, 'Aligned', ['iss' num2str(r-ctn.isLibraryPrep) '_d' num2str(ctn.DownsizeFactor) '.csv']));
            TForm(3,1:2) = TForm(3,1:2)/ctn.DownsizeFactor;
            
            fprintf('%s Transforming %s\n', datetime, ctn.StitchedImages{r,c});
            
            try
                I = imread(ctn.StitchedImages{r,c});
            catch ME
                if contains(ME.identifier, 'NotExist')
                    load([ctn.StitchedImages{r,c} '.mat']);
                    if exist('transformed', 'var')
                        FloatImage = transformed;
                        clear transformed
                    end
                else
                    disp(ME.identifier);
                end
            end
            I = imwarp(I, affine2d(TForm), 'OutputView', imref2d(SizeRef));
            
            try
                imwrite(I, OutputFile);
            catch ME
                if contains(ME.identifier, 'tooMuchData')
                    save([OutputFile '.mat'], 'I', '-v7.3');
                end
            end            
            clear I
        else
            fprintf('%s %s already transformed\n', datetime, ctn.StitchedImages{r,c});
        end
        ctn.AlignedImages{r,c} = OutputFile;
    end
end

ctn.AlignedImages(1,:) = ctn.StitchedImages(1,:);

end


