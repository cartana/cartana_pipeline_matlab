function ctn = quality_check(ctn)
% Quality check and write output files
% Cartana, Xiaoyan, 2020


% quick look at quality
ReadCategory = ones(length(ctn.DecodedResults), 1);
iNNNN = find(strcmp(ctn.DecodedResults(:,2), 'NNNN'));
ReadCategory(iNNNN) = 0;
% homopolymers
ReadCategory(iNNNN(cellfun(@(v) length(unique(v)), ctn.DecodedResults(iNNNN,1))==1)) = -1;
[Thresholds, Counts] = qualitybar(0, 1, ReadCategory, cell2mat(ctn.DecodedResults(:,5)));
drawnow;

% write into a csv file
fid = fopen(fullfile(ctn.OutputDirectory, 'Decoded_LowThreshold.csv'), 'w');
fprintf(fid, 'gene,X,Y\n');
towrite = ctn.DecodedResults(~strcmp(ctn.DecodedResults(:,2), 'NNNN'),2:4)';
fprintf(fid, '%s,%f,%f\n', towrite{:});
fclose(fid);

% write gene count file
[uGenes, ~, iGene] = unique(ctn.DecodedResults(:,2));
cGene = hist(iGene, 1:length(uGenes));
towrite = [uGenes, num2cell(cGene')]';
fid = fopen(fullfile(ctn.OutputDirectory, 'GeneCount_LowThreshold.csv'), 'w');
fprintf(fid, 'Gene,Count\n');
fprintf(fid, '%s,%d\n', towrite{:});
fclose(fid);

% write code count file
[uCodes, fCode, iCode] = unique(ctn.DecodedResults(:,1));
cCode = hist(iCode, 1:length(uCodes));
towrite = [uCodes, ctn.DecodedResults(fCode,2), num2cell(cCode')]';
fid = fopen(fullfile(ctn.OutputDirectory, 'CodeCount_LowThreshold.csv'), 'w');
fprintf(fid, 'Code,Gene,Count\n');
fprintf(fid, '%s,%s,%d\n', towrite{:});
fclose(fid);

% quality bar
fid = fopen(fullfile(ctn.OutputDirectory, 'Qualitybar.csv'), 'w');
fprintf(fid, 'threshold,expected,unexpected,homomer,belowQT\n');
towrite = [Thresholds',Counts];
fprintf(fid,'%.2f,%d,%d,%d,%d\n',towrite');
fclose(fid);


    function [threshold, N] = qualitybar...
            (lower, upper, category, quality, varargin)
        % [threshold, N] = qualitybar...
        %     (lower, upper, category, quality, varargin)
        % draw a bar series to show proportions of
        % expected/unexpected/homomer/below QT at different QTs
        % and give a table with read counts
        %
        % barQuality new
        % Xiaoyan, 2014-11-30
        
        threshold = linspace(lower, upper, 21);
        inter = (upper-lower)/20;
        N = zeros(length(threshold),4);
        
        for i = 1:length(threshold)
            f = category(quality > threshold(i));
            % below Th
            N(i,4) = length(category)-length(f);
            % expected
            N(i,1) = nnz(f~=0 & f~=-1);
            % unexpected
            N(i,2) = nnz(f==0);
            % homomers
            N(i,3) = nnz(f==-1);
        end
        
        
        f = figure; subplot(1, 9, [4 9]);
        h = bar(threshold, N, 0.7, 'stacked');
        axis([lower-inter upper+inter 0 length(category)*1.02]);
        colormap('pink');
        set(gca, 'box', 'off','XTick',lower:inter*4:upper);
        %set(h,'EdgeColor','none');
        set(h, 'EdgeColor', [0.8516    0.6445    0.1250], 'LineWidth', 1.2);
        xlabel('threshold', 'FontSize', 10);
        ylabel('frequency', 'FontSize', 10);
        title('Reads at different thresholds')
        legend({'expected'; 'unexpected'; 'homomer'; 'below QT'});
        
        h = uitable(f, 'data', N, 'ColumnName',...
            {'expected' 'unexpected' 'homomer' 'below QT'},...
            'RowName',threshold);
        set(h, 'units', 'normalized', 'Position', [0 0 0.3 1]);
        set(f, 'name', 'quality bar', 'units', 'normalized',...
            'position', [.2 .15 .5 .7]);
        
    end
end