function ctn = read_microscopy_files(ctn)
% Read images and metadata from microscopy files that are supported by
% bio-formats
% Input files can be with z stacks or already flattened
% If the input has multiple z stacks, maximum intensity projection will be
% calculated
% Cartana, Xiaoyan, 2020


ctn.MicMetadata = cell(length(ctn.BioformatImages),3);
ctn.FlattenedImageFolders = cell(length(ctn.BioformatImages),1);

for r = 1:length(ctn.BioformatImages)
    
    % get metadata
    [nSeries, nSerieswPos, nChannels, nZstacks, xypos, pixelsize, nTseries] = ...
        get_ome_tilepos(ctn.BioformatImages{r});
    
    % Nikon .nd2 files save maximum intensity projection files as time
    % series
    if nSeries==1 && nSerieswPos == 0
        nSerieswPos = nTseries;
    end
    
    % store important metadata
    ctn.MicMetadata(r,1) = {xypos};
    ctn.MicMetadata(r,2) = {nSerieswPos};
    ctn.MicMetadata(r,3) = {[pixelsize, nChannels, nZstacks]};
        
    % output folder
    outdir = fullfile(ctn.OutputDirectory, 'OverlappingTiles', ['iss' num2str(r-ctn.isLibraryPrep)]);
    if ~exist(outdir, 'dir')
        mkdir(outdir);
    end
    
    parfor t = 1:nSerieswPos
        % just a comman line output to show it's running
        % because of parallelization, it does not truthfully represent
        % progress
        if mod(t,100)==1
            fprintf('%s Reading %s Tile %d / %d\n', datetime, ctn.BioformatImages{r}, t, nSerieswPos);
        end
        
        % skip if already exists
        if ~exist(fullfile(outdir, ['t', num2str(t) '.tif']), 'file')
            
            % Initialize a new reader per worker as Bio-Formats is not thread safe
            bfReader = javaObject('loci.formats.Memoizer', bfGetReader(), 0);
            % Initialization should use the memo file cached before
            bfReader.setId(ctn.BioformatImages{r});
            
            if nSeries==1 && nSerieswPos==nTseries
                bfReader.setSeries(0);
            else
                bfReader.setSeries(t-1);
            end
            
            for c = 1:nChannels
                if nZstacks == 1
                    % only one z plane in the file
                    iPlane = bfReader.getIndex(0, c-1, t-1)+1;
                    I = bfGetPlane(bfReader, iPlane);
                    imwrite(I, fullfile(outdir,...
                        ['t', num2str(t) '.tif']),...
                        'tiff', 'writemode', 'append');
                else
                    I = cell(nZstacks,1);
                    for z = 1:nZstacks
                        iPlane = bfReader.getIndex(z-1, c-1, 0)+1;
                        I{z} = bfGetPlane(bfReader, iPlane);
                    end
                     
%                     find contrast OK planes
%                     [bestPlane, okPlanes, topConstrast] = get_infocus_planes(I);
                    
                    % MIP
                    Imip = max(cat(3, I{:}), [], 3);
                    
                    imwrite(Imip, fullfile(outdir,...
                        ['t', num2str(t) '.tif']),...
                        'tiff', 'writemode', 'append');
                end
            end
            bfReader.close();
        end
        
    end
    
    ctn.FlattenedImageFolders(r) = {outdir};
end

ctn.PixelScale = ctn.MicMetadata{end,3}(1);

    function [nSeries, nSerieswPos, nChannels, nZstacks, xyPos,...
            PixelSize, nTseries] = get_ome_tilepos(reader)
        % input: OMERO reader or microscopy image file that contains OMERO metadata
        % Xiaoyan, 2017
        
        
        % get reader if input is file name (character)
        if ischar(reader)
            reader = bfGetReader(reader);
        end
        
        nSeries = reader.getSeriesCount();
        nChannels = reader.getSizeC;
        
        omeMeta = reader.getMetadataStore();
        nZstacks = omeMeta.getPixelsSizeZ(0).getValue();
        PixelSize = double(omeMeta.getPixelsPhysicalSizeX(0).value());
        nTseries = reader.getSizeT;
        
        xyPos  = [];
        for s = 1:nSeries
            if isempty(omeMeta.getPlanePositionX(s-1, 0)) || isempty(omeMeta.getPlanePositionY(s-1, 0))
                %fprintf('Could not load tile positions for series %d\n', i);
            else
                posX = double(omeMeta.getPlanePositionX(s-1, 0).value());
                posY = double(omeMeta.getPlanePositionY(s-1, 0).value());
                xyPos = [xyPos; posX, posY];
                
            end
        end
        
        if ~isempty(xyPos)
            xyPos = bsxfun(@minus, xyPos, min(xyPos,[],1))/PixelSize;
        end
        
        nSerieswPos = size(xyPos,1);
        
        % close
        if ischar(reader)
            reader.close();
        end
        
    end
end

