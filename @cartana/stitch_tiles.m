function ctn = stitch_tiles(ctn)
% Stitch overlapping tiles
% Cartana, Xiaoyan, 2020


% guess how to place tiles
% always assume there is no change between ISS rounds
if isempty(ctn.FovTileMatrix)
    GridConfig = cell(ctn.nRounds + ctn.isLibraryPrep,1);
    for r = 1: (ctn.nRounds + ctn.isLibraryPrep)
        xyPos = ctn.MicMetadata{r};
        
        % get FOV size (assume square sensor)
        TileSize = imfinfo(fullfile(ctn.FlattenedImageFolders{r}, 't1.tif'));
        TileSize = [TileSize.Width];
        TileSize = TileSize(1);
        
        % assume 10% overlap
        GridConfig{r} = tilepos2grid(xyPos, TileSize*.85);
    end
    
    if length(unique(cellfun(@nnz, GridConfig))) == 1 && unique(cellfun(@nnz, GridConfig)) == length(xyPos)
        ctn.FovTileMatrix = GridConfig{end};
    else
        error('Cannot calculate consistent tile position for stitching. Please provide FovTileMatrix property.');
    end
    
end

% stitch images
for r = 1: (ctn.nRounds + ctn.isLibraryPrep)
    
    outdir = fullfile(ctn.OutputDirectory, 'Stitched', ['iss' num2str(r-ctn.isLibraryPrep)]);
    
    if ~exist(outdir, 'dir')
        mkdir(outdir);
    end
    
    imfiles = catstrnum('t', ctn.FovTileMatrix);
    imfiles = strcat(imfiles, '.tif');
    if nnz(ctn.FovTileMatrix==0)
        imfiles(ctn.FovTileMatrix==0) = {'empty.tif'};
        imwrite(zeros(TileSize), fullfile(outdir, 'empty.tif'));
    end
    
    % MIST stitching
    SaveStitched = 1;
    
    % DAPI image
    OutputFile = fullfile(outdir, ['c' num2str(ctn.DAPIChannel(r)) '.stitched-' num2str(ctn.DAPIChannel(r)) '.tif']);
    if ~exist(OutputFile, 'file')
        stitch_time_slice(ctn.FlattenedImageFolders{r}, imfiles,...
            outdir, ['c' num2str(ctn.DAPIChannel(r)) '.'],...
            1, NaN, NaN, 'Overlay', 0,...
            SaveStitched, 0,...
            fullfile(outdir, ['c' num2str(ctn.DAPIChannel(r)) '_log.txt']),...
            10, 10, ctn.DAPIChannel(r));
    else
        fprintf('%s %s already stitched\n', datetime, OutputFile);
    end
    ctn.StitchedImages{r,ctn.DAPIChannel(r)} = OutputFile;
    
    % use the same as DAPI to stitch other channels
    nStacks = length(imfinfo(fullfile(ctn.FlattenedImageFolders{r}, 't1.tif')));
    for c = setdiff(1:nStacks, ctn.DAPIChannel(r))
        AssembleOnly = 1;
        OutputFile = fullfile(outdir, ['c' num2str(c) '.stitched-' num2str(c) '.tif']);
        
        if ~exist(OutputFile, 'file')
            copyfile(fullfile(outdir, ['c' num2str(ctn.DAPIChannel(r)) '.metadata-1.mat']),...
                fullfile(outdir, ['c' num2str(c) '.metadata-1.mat']));
            stitch_time_slice(ctn.FlattenedImageFolders{r}, imfiles,...
                outdir, ['c' num2str(c) '.'],...
                1, NaN, NaN, 'Overlay', 0,...
                SaveStitched, AssembleOnly,...
                fullfile(outdir, ['c' num2str(c) '_log.txt']),...
                10, 10, c);
        else
            fprintf('%s %s already stitched\n', datetime, OutputFile);
        end
        ctn.StitchedImages{r,c} = OutputFile;
    end
    
end


    function gridorder = tilepos2grid(tilepos, tilesize, angle)
        % convert metadata tile position to grid
        % Xiaoyan, 2017
        
        if nargin > 2
            tilepos = ([cos(angle) -sin(angle); sin(angle) cos(angle)]*tilepos')';
            tilepos = tilepos - min(tilepos);
        end
        
        % find stage movement delta pixel
        tilepos = round(tilepos, -2);
        if nargin < 2
            tilesize = unique(tilepos(:));
            tilesize = abs(repmat(tilesize, 1, length(tilesize)) -...
                repmat(tilesize', length(tilesize), 1));
            [~, tilesize] = hist(tilepos(:), unique(tilepos(:)));
            % [~, idx] = sort(ndist, 'descend');
            tilesize = tilesize(2);
        end
        
        % convert to grid
        gridpos = floor(tilepos/tilesize) + 1;
        ntiles = max(gridpos, [], 1);
        
        gridorder = zeros(ntiles);
        gridorder((gridpos(:,2)-1)*ntiles(1) + gridpos(:,1)) = 1:length(tilepos);
        
        % almost all microscope stages move along x axis first
        gridorder = gridorder';
    end

    function strnum = catstrnum(str, numlist)
        % concatenate string and a list of numbers
        % Xiaoyan, 2017
        
        numstr = cellfun(@num2str, num2cell(numlist), 'uni', 0);
        strnum = strcat(str, numstr);
        
    end

end

