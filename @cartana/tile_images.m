function ctn = tile_images(ctn)
% Tile stitched images to get ready to analysis
% These tiled images have no overlap and the size is small enough to
% utilize parallelization
% Cartana, Xiaoyan, 2020


nRounds = ctn.nRounds + ctn.isLibraryPrep;
nChannels = size(ctn.AlignedImages, 2);

[nTilesX, nTilesY, PadX, PadY] = check_images;

% pad and tile images
for r = 1:nRounds
    for c = 1:nChannels
        ImageFile = fullfile(ctn.AlignedImages{r,c});
        
        if exist(ImageFile, 'file')
            outdir = fullfile(ctn.OutputDirectory, 'Tiled',...
                ['iss' num2str(r - ctn.isLibraryPrep)], ['c' num2str(c)]);
            
            if ~exist(outdir, 'dir')
                mkdir(outdir);
            end
            
            if ~exist(fullfile(outdir, 'tile1.tif'), 'file')
                fprintf('%s Tiling %s\n', datetime, ctn.AlignedImages{r,c});
                
                try
                    I = imread(ImageFile);
                catch ME
                    if contains(ME.identifier, 'NotExist')
                        load([ImageFile '.mat']);
                        if exist('transformed', 'var')
                            I = transformed;
                            clear transformed
                        end
                    else
                        disp(ME.identifier);
                    end
                end
                
                I = padimg(I, PadX(r,c), PadY(r,c));
                tileimage(I, ctn.TileSize, outdir);
            else
                fprintf('%s %s already tiled\n', datetime, ctn.AlignedImages{r,c});
            end
            ctn.TileFolders{r,c} = outdir;
        end
    end
end


TilePosX = repmat(0:ctn.TileSize:ctn.TileSize*(nTilesX-1), nTilesY, 1);
TilePosY = repmat((0:ctn.TileSize:ctn.TileSize*(nTilesY-1))', 1, nTilesX);

ctn.TilePosition = [reshape(TilePosX',[],1), reshape(TilePosY',[],1)];


% make synthetic reference image if no library prep image is provided
if ~ctn.isLibraryPrep
    outdir = fullfile(ctn.OutputDirectory, 'Tiled', 'ref');
    
    if ~exist(outdir, 'dir')
        mkdir(outdir);
    end
    
    if ~exist(fullfile(outdir, 'tile1.tif'), 'file')
        for t = 1:(nTilesX * nTilesY)
            I = zeros(ctn.TileSize,ctn.TileSize,4);
            
            for c = ctn.ISSChannels(1,:)
                im = imread(fullfile(ctn.OutputDirectory, 'Tiled', 'iss1',...
                    ['c' num2str(c)], ['tile' num2str(t) '.tif']));
                I(:,:,c) = im;
            end
            
            % take the maximum intensity of images of ISS channels
            imwrite(uint16(max(I, [], 3)),...
                fullfile(outdir, ['tile' num2str(t) '.tif']));
        end
    else
        fprintf('%s %s already tiled\n', datetime, 'reference');
    end
end


    function [ntilesX, ntilesY, padX, padY] = check_images
        ImageDimensions = zeros(nRounds, nChannels, 2);
        for r = 1:nRounds
            for c = 1:nChannels
                if r == 1 && isempty(ctn.AlignedImages{r,c})
                    break
                else
                    f = imfinfo(ctn.AlignedImages{r,c});
                    ImageDimensions(r,c,:) = [f.Width, f.Height];
                end
            end
        end
        maxX = max(reshape(ImageDimensions(:,:,1), [], 1));
        maxY = max(reshape(ImageDimensions(:,:,2), [], 1));
        ntilesX = ceil(maxX/ctn.TileSize);
        ntilesY = ceil(maxY/ctn.TileSize);
        
        padX = ctn.TileSize*ntilesX - ImageDimensions(:,:,1);
        padY = ctn.TileSize*ntilesY - ImageDimensions(:,:,2);
    end

    function tileimage(im, tilesize, outdir)
        % create non-overlapping image tiles
        % Xiaoyan, 2017
        
        imsize = size(im);
        
        ntileX = ceil(imsize(2)/tilesize);
        ntileY = ceil(imsize(1)/tilesize);
        
        % pad image
        im = padimg(im, tilesize*ntileX-imsize(2), tilesize*ntileY-imsize(1), 'SE');
        
        % tile and save
        for i = 1:ntileY
            for j = 1:ntileX
                xstart = tilesize*(j-1);
                ystart = tilesize*(i-1);
                tile = im(ystart+1:ystart+tilesize, xstart+1:xstart+tilesize, :);
                imwrite(tile,...
                    [outdir, '\tile' num2str(ntileX*(i-1)+j), '.tif']);
            end
        end
        
    end

    function im = padimg(im, deltaX, deltaY, sides)
        % im = padimg(im, deltaX, deltaY, sides)
        % pad image with zeros
        % crop when deltaX or deltaY are smaller than zero
        % if sides not specified, pad right and lower
        % Xiaoyan, 2017
        
        
        imsize = size(im);
        try
            imsize(3);
        catch
            imsize(3) = 1;
        end
        
        if deltaX > 0
            padX = zeros(imsize(1), deltaX, imsize(3));
        end
        
        if deltaY > 0
            padY = zeros(deltaY, imsize(2)+deltaX, imsize(3));
        end
        
        if nargin < 4
            sides = 'ES';
        end
        
        % pad/crop different side
        if strfind(sides, 'E')
            try im = [im, padX];
            catch
                im = im(:,1:imsize(2)+deltaX,:);
            end
        end
        
        if strfind(sides, 'W')
            try im = [padX, im];
            catch
                im = im(:,abs(deltaX)+1:end,:);
            end
        end
        
        if strfind(sides, 'S')
            try im = [im; padY];
            catch errorm
                if strfind(errorm.identifier, 'dimensionMismatch')
                    padY = zeros(deltaY, imsize(2)+2*deltaX, imsize(3));
                    im = [im; padY];
                else
                    im = im(1:imsize(1)+deltaY,:,:);
                end
            end
        end
        
        if strfind(sides, 'N')
            try im = [padY; im];
            catch errorm
                if strfind(errorm.identifier, 'dimensionMismatch')
                    padY = zeros(deltaY, imsize(2)+2*deltaX, imsize(3));
                    im = [im; padY];
                else
                    im = im(abs(deltaY)+1:end,:,:);
                end
            end
        end
        
    end


end