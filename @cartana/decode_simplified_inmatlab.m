function ctn = decode_simplified_inmatlab(ctn)
% Segmentation and decoding in MATLAB
% Simplified version withou sophisticated declustering
% Cartana, Xiaoyan, 2020


code = importdata(ctn.TaglistFile, ',');
code = cellfun(@(v) strsplit(v, ','), code, 'UniformOutput', 0);
code = cat(1, code{:});
genes = code(:,end);
code = code(:,1);

ctn.Taglist = [code, genes];

DecodedTileResults = cell(size(ctn.TileAnalysisInput,1),3);

% clear any previously saved results
ctn.DecodedResults = [];


% can parallelize if padimg function is standalone

if isprop(ctn,'SaveIntermediate') && ctn.SaveIntermediate
    mkdir(fullfile(ctn.OutputDirectory, 'TileProcessing'));
end


parfor t = 1:size(ctn.TileAnalysisInput,1)
    
    fprintf('%s Analyzing tile %d / %d\n', datetime, t, size(ctn.TileAnalysisInput,1));

    % read reference spot image
    ReferenceImage = imread(ctn.TileAnalysisInput{t,9,1});
    
    % a quick thresholding plus maxima detection (threshold arbitrary for now)
    % TODO: auto threshold
    % TODO: automatic tophat filter size based on pixel scaling
    
    spotmax = ReferenceImage==imdilate(ReferenceImage, strel('disk', 2)) & ReferenceImage>ctn.MinSignalIntensity;
    spotidx = find(spotmax);
    
    % label binary image
    spotim = zeros(ctn.TileSize, ctn.TileSize, 'uint32');
    spotim(spotidx) = 1:length(spotidx);
    
    % suppress maxima within a short distance
    spotimdil = imdilate(spotim, strel('disk', 2));
    spotidx(ismember(spotidx, find(spotim~= spotimdil))) = [];
    spotim(spotim ~= spotimdil) = 0;

    [y,x] = ind2sub(size(ReferenceImage), spotidx);
    
    basecall = zeros(length(y), ctn.nRounds, 2);
    for r = 1:ctn.nRounds
        Istack = [];
        
        % read individual channel images of a certain imaging round
        for c = 1:4
            I = imread(ctn.TileAnalysisInput{t,c+4,r});
            Istack = cat(3, Istack, I);
        end
        
        % combine all channels (take maximum value) to create an image that contains all signals
        FloatImage = max(Istack, [], 3);
        
        %         % DFTregistration
        %         [tform, ~] = dftregistration(fft2(ReferenceImage), fft2(FloatImage), 2);
        
        % registration (pixel resolution)
        tform = dftregistration(fft2(ReferenceImage), fft2(FloatImage), 1);
        
        % tophat filter channel images and register (ignore any rotation), and return registered images in the same size as input
        for c = 1:4
            I = imtophat(Istack(:,:,c), strel('disk', 3));
            I = padimg(I, round(tform(4)), round(tform(3)), 'NW');
            I = padimg(I, ctn.TileSize-size(I,2), ctn.TileSize-size(I,1));
            Istack(:,:,c) = I;
        end
        
        Istack = double(Istack);
        
        if isprop(ctn,'SaveIntermediate') && ctn.SaveIntermediate
            Ifuse = imfuse(ReferenceImage, max(Istack, [], 3));
            Ifuse(spotidx) = 255;
            imwrite(Ifuse, fullfile(ctn.OutputDirectory, 'TileProcessing',...
                ['t' num2str(t) '_r' num2str(r) '.tif']));
        end
%         clf; imshowpair(ReferenceImage, max(Istack, [], 3));

        % take maximum intensity corresponding base
        [BaseIntensity, base] = max(Istack, [], 3);
        SumIntensity = sum(Istack, 3);
        basecall(:,r,1) = base(spotidx);
        basecall(:,r,2) = BaseIntensity(spotidx)./SumIntensity(spotidx);
        
%         clear Istack
    end
    
    
    if ~isempty(basecall)
        bases = cellstr(num2str(basecall(:,:,1)));
        bases = cellfun(@(v) strrep(v, ' ', ''), bases, 'UniformOutput', 0);
        quality = min(basecall(:,:,2), [], 2);
        gene = repmat({'NNNN'}, length(bases), 1);
        try
            gene(ismember(bases, code)) = genes(cellfun(@(v) find(strcmp(v, code)), bases(ismember(bases, code))));
        end
        DecodedTileResults(t,:) = [{[x,y]}, {[bases, gene]}, {quality}];
%         clf; plotall(gene, [x,y], ReferenceImage); pause()
    end
end
    
for t = 1:size(ctn.TileAnalysisInput,1)
    try
        ctn.DecodedResults = [ctn.DecodedResults;
            DecodedTileResults{t,2},...
            num2cell(DecodedTileResults{t,1} + cell2mat(ctn.TileAnalysisInput(t,2:3))),...
            num2cell(DecodedTileResults{t,3})];
    catch
        fprintf('Analyzing tile %d \n',t)
    end
end

quality_check(ctn);

%     function im = padimg(im, deltaX, deltaY, sides)
%         % im = padimg(im, deltaX, deltaY, sides)
%         % pad image with zeros
%         % crop when deltaX or deltaY are smaller than zero
%         % if sides not specified, pad right and lower
%         % Xiaoyan, 2017
%         
%         
%         imsize = size(im);
%         try
%             imsize(3);
%         catch
%             imsize(3) = 1;
%         end
%         
%         if deltaX > 0
%             padX = zeros(imsize(1), deltaX, imsize(3));
%         end
%         
%         if deltaY > 0
%             padY = zeros(deltaY, imsize(2)+deltaX, imsize(3));
%         end
%         
%         if nargin < 4
%             sides = 'ES';
%         end
%         
%         % pad/crop different side
%         if strfind(sides, 'E')
%             try im = [im, padX];
%             catch
%                 im = im(:,1:imsize(2)+deltaX,:);
%             end
%         end
%         
%         if strfind(sides, 'W')
%             try im = [padX, im];
%             catch
%                 im = im(:,abs(deltaX)+1:end,:);
%             end
%         end
%         
%         if strfind(sides, 'S')
%             try im = [im; padY];
%             catch errorm
%                 if strfind(errorm.identifier, 'dimensionMismatch')
%                     padY = zeros(deltaY, imsize(2)+2*deltaX, imsize(3));
%                     im = [im; padY];
%                 else
%                     im = im(1:imsize(1)+deltaY,:,:);
%                 end
%             end
%         end
%         
%         if strfind(sides, 'N')
%             try im = [padY; im];
%             catch errorm
%                 if strfind(errorm.identifier, 'dimensionMismatch')
%                     padY = zeros(deltaY, imsize(2)+2*deltaX, imsize(3));
%                     im = [im; padY];
%                 else
%                     im = im(abs(deltaY)+1:end,:,:);
%                 end
%             end
%         end
%         
%     end


end