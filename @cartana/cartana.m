classdef cartana
    properties 
        
        
        % output directory, subfolders will be created 
        OutputDirectory;
        
        % if library prep image is required to be as reference image
        isLibraryPrep = 0;
        
        % number of fluorescence channels and imaging rounds for ISS
        nChannels = 4;
        nRounds = 6;
        
        % channel number of Alex Fluor 750, Cy3, Cy5, AF488
        ISSChannels = repmat([1 4 3 2], 6, 1);
        
        % DAPI channel number in different ISS cycles
        DAPIChannel = ones(6,1)*5;
        
        % channel number of anchor in library prep imag
        AnchorChannel;
        
        % pixel scaling (default 20x magnification, 6.5um camera pixel size)
        % will be overwritten if input are microscopy files
        PixelScale = 0.325;
        
        % path to microscopy files, assuming the files contain OMERO
        % metadata and individual FOV images
        % the order matches ISS imaging order, with library prep coming
        % first
        BioformatImages;
        
        % folders with 2D flattened microscopy FOV images in multi-stack tiff
        % one folder for one ISS round
        % file name: t1.tif, t2.tif,...
        % each stack corresponds to fluorescence channels
        FlattenedImageFolders;
        
        % path to stitched image files, ISS rounds x channels
        StitchedImages;
        
        % path to more or less pre-aligned images, ISS rounds x channels
        AlignedImages;
        
        % path to starfish-formatted files
        StarfishPath;
        
        % path to ready-to-basecall image files
        TileFolders;
        
        % microscopy metadata
        MicMetadata;
        
        % information about how to place tiles to make stitched images
        FovTileMatrix;

        % how much downsize in prealign step
        DownsizeFactor;
        
        % the size of each tile for analysis, default: 2000 x 2000
        TileSize = 2000;
        
        % tile starting postion after an image is tiled without overlap
        TilePosition;
        
        % input specified for analysis and basecalling
        TileAnalysisInput;
        
        % code book
        Codebook;
        
        % the path to taglist: simplified version of codebook
        % 1 = AF750, 2 = Cy5, 3 = Cy3, 4 = AF488
        TaglistFile;
        
        % taglist: simplified version of codebook
        % 1 = AF750, 2 = Cy5, 3 = Cy3, 4 = AF488
        Taglist;
        
        % absolute minimal intensity level to be considered as signals
        MinSignalIntensity;
        
        % decoded results
        DecodedResults;
        
        % save more intermediate output
        SaveIntermediate = 0;
        
    end
end



        
        