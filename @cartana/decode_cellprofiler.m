function ctn = decode_cellprofiler(ctn)
% Decode results from Cellprofiler
% This is simplified version with many error catching and additional
% options omitted
% Not all information is used either, e.g. alignment score
% Cartana, Xiaoyan, 2021


code = importdata(ctn.TaglistFile, ',');
code = cellfun(@(v) strsplit(v, ','), code, 'UniformOutput', 0);
code = cat(1, code{:});
genes = code(:,end);
code = code(:,1);

ctn.Taglist = [code, genes];

disp('Loading Cellprofiler results..')
try
    CPResults = importdata(fullfile(ctn.OutputDirectory, 'Cellprofiler', 'blobs.csv'));
catch
    CPResults = importdata(fullfile(ctn.OutputDirectory, 'blobs.csv'));
end    
CPResults = CPResults.data;

% clear any previously saved results
ctn.DecodedResults = [];

DecodedTileResults = cell(size(ctn.TileAnalysisInput,1),3);

for t = unique(CPResults(:,3))'
    TileData = CPResults(CPResults(:,3)==t,:);
    [~, fSpot, iSpot] = unique(TileData(:,2));
    [~, order] = sort(iSpot);
    
    [BaseIntensity, base] = max(TileData(order,6:9), [], 2);
    SumIntensity = sum(TileData(order,6:9), 2);
    basecall = cat(3,...
        reshape(base, ctn.nRounds, [])',...
        reshape(BaseIntensity./SumIntensity, ctn.nRounds, [])');
    
    only_these = all(~isnan(basecall(:,:,2)),2);
    
    if nnz(only_these)
        
        x = TileData(fSpot,10);
        y = TileData(fSpot,11);
        
        bases = cellstr(num2str(basecall(:,:,1)));
        bases = cellfun(@(v) strrep(v, ' ', ''), bases, 'UniformOutput', 0);
        quality = min(basecall(:,:,2), [], 2);
        gene = repmat({'NNNN'}, length(bases), 1);
        
        x = x(only_these);
        y = y(only_these);
        bases = bases(only_these);
        quality = quality(only_these);
        gene = gene(only_these);
        
        try
            gene(ismember(bases, code)) = genes(cellfun(@(v) find(strcmp(v, code)), bases(ismember(bases, code))));
        end
        DecodedTileResults(t,:) = [{[x,y]}, {[bases, gene]}, {quality}];
        %     clf; plotall(gene, [x,y], ''); pause()
        
        ctn.DecodedResults = [ctn.DecodedResults;
            bases, gene,...
            num2cell([x,y] + cell2mat(ctn.TileAnalysisInput(t,2:3))),...
            num2cell(quality)];
    end
    
end

quality_check(ctn);

end
