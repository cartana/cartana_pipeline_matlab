function ctn = specify_input(ctn)
% Make a file to specify input for analysis
% The same file can be used for Cellprofiler
% Cartana, Xiaoyan, 2020

% clear previous saves
ctn.TileAnalysisInput = [];

% reference image folder
if ctn.isLibraryPrep
    reference = ctn.TileFolders{1,ctn.AnchorChannel};
else
    reference = fullfile(ctn.OutputDirectory, 'Tiled', 'ref');
end

% already sort all images using the channel information
for t = 1:size(ctn.TilePosition,1)
    for r = 1:ctn.nRounds
        for b = 1:4
            ctn.TileAnalysisInput{t,b,r} = fullfile(ctn.TileFolders{r + ctn.isLibraryPrep,ctn.ISSChannels(r,b)}, ['tile' num2str(t) '.tif']);
        end
        ctn.TileAnalysisInput{t,5,r} = fullfile(reference, ['tile' num2str(t) '.tif']);
    end
end

% add metadata: tile number, tile position and ISS cycle number
ctn.TileAnalysisInput = [num2cell(repmat([(1:size(ctn.TilePosition,1))', ctn.TilePosition], 1, 1, ctn.nRounds)),...
    num2cell(reshape(repmat(1:ctn.nRounds, size(ctn.TilePosition,1), 1), [], 1, ctn.nRounds)),...
    ctn.TileAnalysisInput];

% write csv file for cellprofiler input
towrite = permute(ctn.TileAnalysisInput, [2 1 3]);

mkdir(fullfile(ctn.OutputDirectory, 'Cellprofiler'));

fid = fopen(fullfile(ctn.OutputDirectory, 'Cellprofiler', 'InputForCP.csv'), 'w');
fprintf(fid, 'Metadata_Position,Tile_xPos,Tile_yPos,CycleNumber,Image_FileName_DO1,Image_FileName_DO2,Image_FileName_DO3,Image_FileName_DO4,Image_FileName_anchor\n');
fprintf(fid, [strjoin([repmat({'%d'}, 1, 4), repmat({'%s'}, 1, 5)], ','), '\n'], towrite{:});
fclose(fid);



end






