% This is a pipeline to process ISS example images all way from the
% beginning, i.e. raw acquisition files.
% The provided files are in Nikon .nd2 format, which supports bio-formats
% Cartana, Xiaoyan, 2020



% Initiate
ctn = cartana;
ctn.OutputDirectory = 'G:\Output';

% How many rounds of ISS cycles
ctn.nRounds = 5;

% Whether library prep image is provided and required to be used as reference
% If no library preparation image was taken, the value is 0, and ISS cycle 1
% images will be used as reference instead
ctn.isLibraryPrep = 1;

% The channel number of anchor stain in library prep image
% If library prep image anchor and DAPI images are provided, this image
% will be used as reference ISS spots
ctn.AnchorChannel = 2;

% DAPI channel, one for each ISS cycle, Lirary Prep is the first
ctn.DAPIChannel = [1 5 5 5 5 5]; 

% ISS channels
% One row for one cycle of ISS, 4 channels in each cycle
% Cycle order: AlexFluor750, Cy5, Cy3, AlexFluor488 (same in all cycles). This is to rearrange the channels to be consistent with the long2short taglist.
% In this case, AlexFluor750, Cy5, Cy3, AlexFluor488 is 1234 in the taglist. The ACGT in QC script will be for AlexFluor750, Cy5, Cy3, AlexFluor488 separately.
ctn.ISSChannels = repmat([1 3 4 2], 5, 1);    


%% Pre-align stitched images 
% To compensate for inaccuracy in microscope stage repositioning
ctn.StitchedImages = {...
    'LibPrep_channel1.tif', 'LibPrep_channel2.tif', '', '', '';
    'Cycle1_channel1.tif', 'Cycle1_channel2.tif', 'Cycle1_channel3.tif', 'Cycle1_channel4.tif', 'Cycle1_channel5.tif';
    'Cycle2_channel1.tif', 'Cycle2_channel2.tif', 'Cycle2_channel3.tif', 'Cycle2_channel4.tif', 'Cycle2_channel5.tif';
    'Cycle3_channel1.tif', 'Cycle3_channel2.tif', 'Cycle3_channel3.tif', 'Cycle3_channel4.tif', 'Cycle3_channel5.tif';
    'Cycle4_channel1.tif', 'Cycle4_channel2.tif', 'Cycle4_channel3.tif', 'Cycle4_channel4.tif', 'Cycle4_channel5.tif';
    'Cycle5_channel1.tif', 'Cycle5_channel2.tif', 'Cycle5_channel3.tif', 'Cycle5_channel4.tif', 'Cycle5_channel5.tif'};
ctn = prealign_sections(ctn);

%% Tile images into a size that is easy to handle
% These can also be input files for starfish pipeline
ctn = tile_images(ctn);

%% Prepare matrix to specify file 
ctn = specify_input(ctn);

save(fullfile(ctn.OutputDirectory, 'FilesReady.mat'), 'ctn');

%% Use Cellprofiler pipeline to 
% Run cellprofiler using the InputForCP.csv in the first module

% reload variables
load(fullfile(ctn.OutputDirectory,'FilesReady.mat'))

% decodeing
% provide the path to codebook
ctn.TaglistFile = 'G:\taglist_TV307_incomplete.csv';
ctn.SaveIntermediate = 1;
ctn = decode_cellprofiler(ctn);

% save MATLAB variable file, but may take a long time
save(fullfile(ctn.OutputDirectory, 'CP.mat'), 'ctn', '-v7.3');

% % or use MATLAB
% ctn.MinSignalIntensity = 400; % minimal intensity level in the original image to be considered above background
% ctn = decode_simplified_inmatlab(ctn);

%% plot again
plot_reads(ctn)
