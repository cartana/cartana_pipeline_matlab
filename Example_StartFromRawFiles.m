% This is a pipeline to process ISS example images all way from the
% beginning, i.e. raw acquisition files.
% The provided files are in Nikon .nd2 format, which supports bio-formats
% Cartana, Xiaoyan, 2020



% Initiate
ctn = cartana;
ctn.OutputDirectory = 'G:\Output';

% How many rounds of ISS cycles
ctn.nRounds = 5;

% Whether library prep image is provided and required to be used as reference
% If no library preparation image was taken, the value is 0, and ISS cycle 1
% images will be used as reference instead
ctn.isLibraryPrep = 1;

% The channel number of anchor stain in library prep image
% If library prep image anchor and DAPI images are provided, this image
% will be used as reference ISS spots
ctn.AnchorChannel = 1;

% DAPI channel, one for each ISS cycle, Lirary Prep is the first
ctn.DAPIChannel = [2 5 5 5 5 5]; 

% ISS channels
% One row for one cycle of ISS, 4 channels in each cycle
% Cycle order: AlexFluor750, Cy3, Cy5, AlexFluor488 (same in all cycles)
ctn.ISSChannels = repmat([1 3 4 2], 5, 1);    

%% Process raw microscopy files - output subfolder: OverlappingTiles
% The order follows ISS order. If there is library prep image, always put
% it first.
% If input files have multiple z planes, maximum intensity preojction will
% be calculated
% If input files have only one z plane, it will simply make a copy of
% images and put them into a more convenient format
ctn.BioformatImages = {...
    'G:\Input\NDSequence002_anchor.nd2'
    'G:\Input\NDSequence002_cycle1.nd2'
    'G:\Input\NDSequence002_cycle2.nd2'
    'G:\Input\NDSequence002_cycle3.nd2'
    'G:\Input\NDSequence002_cycle4.nd2'
    'G:\Input\NDSequence002_cycle5.nd2'};

ctn = read_microscopy_files(ctn);

%% Stitch the section images - output subfolder: Stitched
% The microscope stage movemont not straight, easier to provide tile
% position, although there is an embedded function to calculate it

% % automatically determine tile position (assume FOV size is ~ 2000px and
% % 10% overlap)
% tilepos = round(ctn.MicMetadata{1,1} , -2);
% 
% % convert to grid
% gridpos = floor(tilepos/(2000*.9)) + 1;
% ntiles = max(gridpos, [], 1);
% 
% gridorder = zeros(ntiles);
% gridorder((gridpos(:,2)-1)*ntiles(1) + gridpos(:,1)) = 1:size(tilepos,1);
% 
% % almost all microscope stages move along x axis first
% ctn.FovTileMatrix = gridorder'; 

ctn.FovTileMatrix = reshape(1:100,10,10)';
ctn.FovTileMatrix(2:2:end,:) = fliplr(ctn.FovTileMatrix(2:2:end,:));
% the code above makes
% ctn.FovTileMatrix = 
%      1     2     3     4     5     6     7     8     9    10
%     20    19    18    17    16    15    14    13    12    11
%     21    22    23    24    25    26    27    28    29    30
%     40    39    38    37    36    35    34    33    32    31
%     41    42    43    44    45    46    47    48    49    50
%     60    59    58    57    56    55    54    53    52    51
%     61    62    63    64    65    66    67    68    69    70
%     80    79    78    77    76    75    74    73    72    71
%     81    82    83    84    85    86    87    88    89    90
%    100    99    98    97    96    95    94    93    92    91
ctn = stitch_tiles(ctn);

%% Fix ISS cycle 1 issue
% % I don't know why but ISS cycle 1 in the given example somehow is upside
% % down. The microscope software sometimes is a bit funky.
% % In order to process ISS cycle 1, you need to delete the folder "iss1" in
% % the folder Stitched and run the following code again. It will essentially
% % run the stitching again but ignoring all files that already have output.
% ctn.FovTileMatrix = reshape(1:100,10,10)';
% ctn.FovTileMatrix(2:2:end,:) = fliplr(ctn.FovTileMatrix(2:2:end,:));
% ctn.FovTileMatrix = flipud(ctn.FovTileMatrix);
% % the code above makes
% % ctn.FovTileMatrix = 
% %    100    99    98    97    96    95    94    93    92    91
% %     81    82    83    84    85    86    87    88    89    90
% %     80    79    78    77    76    75    74    73    72    71
% %     61    62    63    64    65    66    67    68    69    70
% %     60    59    58    57    56    55    54    53    52    51
% %     41    42    43    44    45    46    47    48    49    50
% %     40    39    38    37    36    35    34    33    32    31
% %     21    22    23    24    25    26    27    28    29    30
% %     20    19    18    17    16    15    14    13    12    11
% %      1     2     3     4     5     6     7     8     9    10
% ctn = stitch_tiles(ctn);
% 
% % And now flip the images again so that they are the same as the rest of
% % the ISS cycles. This part is a specific fix to this set of images, please
% % delete if you are running it on your own images. This will replace the
% % original output from stitching.
% for c = 1:5
%     I = imread(ctn.StitchedImages{2,c});
%     imwrite(flipud(I), ctn.StitchedImages{2,c});
% end


%% Pre-align stitched images 
% To compensate for inaccuracy in microscope stage repositioning
ctn = prealign_sections(ctn);

%% Tile images into a size that is easy to handle
% These can also be input files for starfish pipeline
ctn = tile_images(ctn);

%% Prepare matrix to specify filepath
ctn = specify_input(ctn);

save(fullfile(ctn.OutputDirectory, 'FilesReady.mat'), 'ctn');

%% Run CellProfiler pipeline next 
% Run cellprofiler using the InputForCP.csv in the first module

% decodeing
ctn.TaglistFile = 'G:\taglist_TV307_incomplete.csv';
ctn = decode_cellprofiler(ctn);

% save MATLAB variable file, but may take a long time
save(fullfile(ctn.OutputDirectory, 'CP.mat'), 'ctn', '-v7.3');

%% Altertively, a simpler version of segmentation and basecalling can be run using cartana class
% This is for someone more familiar with MATLAB to understand the design
% The parameters are not specifically tuned
ctn.TaglistFile = 'G:\taglist_TV307_incomplete.csv';
ctn.MinSignalIntensity = 400; % minimal intensity level in the original image to be considered above background
ctn.SaveIntermediate = 1; % will save all intermediate tile registration files for debugging
ctn = decode_simplified_inmatlab(ctn);

%% plot again
plot_reads(ctn)
